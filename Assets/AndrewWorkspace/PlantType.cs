public class PlantType
{  

    public int WaterConsumption; //water consumed each growth cycle
    public int GrowthTime; //how long a growth cycle is
    public int RotPoint; //point below 0 where plant rots (negative int)
    public int Price; //how much it sells for
    public string PlantName; //Name of the plant
    public string PlantDescription; //Description of plant
    public PlantType(int waterConsumption, int growthTime, int rotPoint, int price, string plantName, string plantDescription)
   {
      WaterConsumption = waterConsumption;
      GrowthTime = growthTime;
      RotPoint = rotPoint;
      Price = price;
      PlantName = plantName;
      PlantDescription = plantDescription;
   }
}
