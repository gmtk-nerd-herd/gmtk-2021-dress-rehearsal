using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlantController : MonoBehaviour
{
    [Header("Plant Stats")]
    public int waterConsumption;
    public int requiredTicks;
    public int rotTicks; 
    public int price;
    [Header("Plant State")]
    public int plantState; // 3:dead, 0:seed, 1:growing, 2:ripe, 3:rotten
    int maxWaterLevel;
    int waterLevel;
    int growthRemaining;
    int rotPoint;
    float growthCycleLength;
    public PlantType plantType;
    public GameObject[] gameObjectMesh;

    public AudioClip growAudio;
    public AudioClip deathAudio;
    private int currentRotTicks;
    private bool isDead=false;

    private GroundSystem groundSystem;


    // Start is called before the first frame update
    void Start()
    {
        // Initializing variables
        plantState = 0;
        maxWaterLevel = 10;
        //NEEDS UPDATE
        //take this from parent somehow
        waterLevel = 10;
        growthCycleLength = 3f;
        currentRotTicks = 0;
        // Copying variables from type
        plantType = new PlantType(waterConsumption, requiredTicks, rotTicks, price, "Sage", "Yummy and smelly");

        growthRemaining = plantType.GrowthTime;
        rotPoint = plantType.RotPoint;
        revealThisMesh(0);
        InvokeRepeating("GrowPlant", growthCycleLength, growthCycleLength);
        
    }
    private void Awake() {
        groundSystem = transform.parent.gameObject.GetComponent<GroundSystem>();
    }
    void GrowPlant()
    {
        // Grow plant based on water level  
        // If the plant hasn't started rotting yet
        // Consume water
        if(isDead) {
            updatePlant(3);
            return;
        };
        if (currentRotTicks == 0 )
        {
            //Debug.Log("Im drinking");
            groundSystem.decayWater(plantType.WaterConsumption);
            growthRemaining--;
        }
        
        //Determing Plant State
        if (growthRemaining > 0)
        {
            if(growthRemaining > plantType.GrowthTime/2){
                updatePlant(0);
            } else {
                updatePlant(1);
            }
            //Debug.Log("I'm Growing");
            
        }
        else if (currentRotTicks == rotPoint)
        {
            //Debug.Log("I Rotted");
            kill();
            
        }
        else
        {
            //Debug.Log("Fully grown and Rotting!");
            currentRotTicks++;
            updatePlant(2);
        }
    }

    private void updatePlant(int i)
    {

        if (i != plantState)
        {
            revealThisMesh(i);
            if(i == 2){
                GetComponent<AudioSource>().PlayOneShot(growAudio,0.8f);
            }
        }
        plantState = i;

    }

    private void revealThisMesh(int i)
    {

        for (int x = 0; x < 4; x++)
        {
            if (x == i)
            {

                gameObjectMesh[x].GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {

                gameObjectMesh[x].GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
    // Called on death
    public void kill()
    {
        if(isDead) return;
        isDead = true;
        updatePlant(3);
        GetComponent<AudioSource>().PlayOneShot(deathAudio,0.8f);
        Debug.Log("I DED");
    }

}
