using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentToolDisplay : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite plantTool;
    public Sprite waterTool;
    public Sprite harvestTool;

    public MouseInputHandler inputReference;
    private int selectedTool;
    private Image imageToUpdate;
    void Start()
    {
        selectedTool = 3;
        imageToUpdate = GetComponent<Image>();
    }

    // Update is called once per frame
    // Ineficient if its not efficient than fix later
    void Update()
    {
        selectedTool = inputReference.getTool();
        updateIcon();
    }

    private void updateIcon()
    {
        switch (selectedTool)
        {
            case 0:
                imageToUpdate.sprite = plantTool;
                break;
            case 1:
                imageToUpdate.sprite = waterTool;
                //Insert Water Logic Here
                break;
            case 2:
                imageToUpdate.sprite = harvestTool;
                //Insert Harvesting Logic Here
                break;
            default:
                print("Theres no tool tied to this value");
                break;
        }
    }
}
