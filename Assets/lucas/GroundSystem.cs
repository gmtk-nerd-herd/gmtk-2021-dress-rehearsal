using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroundSystem : MonoBehaviour
{
    private bool isActive = false;
    public float startingWaterLevel = 10;

    public Material[] dirtMaterials;
    public GameObject[] plants;
    public Canvas WaterBar;
    [System.NonSerialized] public float waterLevel;

    [System.NonSerialized] private bool hasPlant = false;
    public AnimationCurve animationType;
    private GameObject plant;
    public int waterState; //0 fully watered, 1 med, 2 dry

    private bool hovered = false;
    public AudioClip sow;
    public AudioClip reap;
    public AudioClip pop;

    // Start is called before the first frame update
    void Start()
    {
        waterLevel = startingWaterLevel;
        WaterBar.enabled=false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void activate()
    {
        if (!isActive)
        {
            isActive = true;
            Vector3 newPostition = transform.position + Vector3.up;
            float completionTime = Random.Range(.25f, 1.5f);
            LeanTween.delayedCall(completionTime*0.2f,popSoundPlay);
            LeanTween.move(gameObject, newPostition, completionTime).setEase(animationType);
        }
    }
    void popSoundPlay(){
        GetComponent<AudioSource>().PlayOneShot(pop,.5f);
    }

    void killPlant()
    {
        if (hasPlant)
        {
            //todo kill plant function
            plant.GetComponent<PlantController>().kill();
        }
    }

    public bool sowPlant(int plantType)
    {
        if (hasPlant || !isActive)
        {
            return false;
        }

        if (plantType < plants.Length)
        {
            plant = Instantiate(plants[plantType], transform);
            plant.transform.localPosition = new Vector3(0, 1.0f, 0);
            hasPlant = true;
            
            GetComponent<AudioSource>().PlayOneShot(sow,1);
            return true;
        }

        return false;
    }

    public int harvestPlant()
    {
        if (hasPlant)
        {
            //todo get value from plant
            PlantController plantScript = plant.GetComponent<PlantController>();
            Debug.Log(plantScript);

            int value = plantScript.plantType.Price;

            if (plantScript.plantState == 3) value = 0;

            value = value * (plantScript.plantState / 2);

            Destroy(plant);
            hasPlant = false;
            GetComponent<AudioSource>().PlayOneShot(reap,1);

            return value;
        }
        else
        {
            return -1;
        }
    }

    public void water()
    {
        waterLevel = startingWaterLevel;
        checkWaterState();
    }

    public float getWaterLevel()
    {
        return waterLevel;
    }
    public void decayWater(float amount = 1.0f)
    {
        waterLevel -= amount;
        checkWaterState();
    }

    void checkWaterState() {
        if(waterLevel>startingWaterLevel*0.5){
            changeWaterState(0);
        } else if(waterLevel>0){
            changeWaterState(1);
        }
        else
        {
            changeWaterState(2);
        }
    }
    void changeWaterState(int state)
    {
        if (state != waterState)
        {
            swapMaterial(state);
            Debug.Log(state);
            waterState = state;
            if (hasPlant && state == 2)
            {
                plant.GetComponent<PlantController>().kill();
            }
        }
    }

    void swapMaterial(int material)
    {
        GetComponent<Renderer>().material = dirtMaterials[material];
    }

    public void hoverOn() {
        if(!hovered && isActive){
            GetComponent<Renderer>().material.SetColor("_Color", new Color(1,0,0,1));
            WaterBar.enabled=true;
            transform.position = new Vector3(transform.position.x,1.2f,transform.position.z);
            hovered = true;
        }

    }
    public void hoverOff()
    {
        if (hovered)
        {
            GetComponent<Renderer>().material = dirtMaterials[waterState];
            WaterBar.enabled=false;
            transform.position = new Vector3(transform.position.x,1,transform.position.z);
            hovered = false;
        }
    }
}
