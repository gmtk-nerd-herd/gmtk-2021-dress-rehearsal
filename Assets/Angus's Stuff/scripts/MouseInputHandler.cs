using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseInputHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public Dictionary<string,int> seedPrices = new Dictionary<string, int>();
    public Dictionary<string,GameObject> seedButtons = new Dictionary<string, GameObject>();
    public GameObject[] seedButtonsIn;
    public Dictionary<string,int> seedID = new Dictionary<string, int>();
    private Camera cam;
    public GameObject SeedBar;
    public GameObject expandPanel;
    public Text GoldText;
    public GameObject gridSystem;
    private int gold;
    public LayerMask layerMask;
    private string plantType="null";
    public GameObject hoveredDirtBlock;
    private int currentTool = 3;    
    private int expansionCost = 51;

    void Start()
    {
        cam = Camera.main;
        gold=10000;
        GoldText.text = gold+"g";
        toggleSeedBar();

        // Setthemseedprices
        seedPrices.Add("Carrot",2);
        seedPrices.Add("Cabbage",3);
        seedPrices.Add("Turnip",4);
        seedPrices.Add("Potato",1);
        seedPrices.Add("Corn",5);

        // Set seed index CHANGE THIS
        seedID.Add("Carrot",0);
        seedID.Add("Cabbage",1);
        seedID.Add("Turnip",2);
        seedID.Add("Potato",3);
        seedID.Add("Corn",4);


        // Set Seed Buttons
        foreach (KeyValuePair<string, int> entry in seedID){
            seedButtons.Add(entry.Key,seedButtonsIn[entry.Value]);
        }
        
        // Set Seed Button Costs
        foreach (KeyValuePair<string, GameObject> entry in seedButtons){
            entry.Value.GetComponentInChildren<Text>().text=seedPrices[entry.Key]+"g";
        }

        // Activate expansion panel (or not)
        expandPanel.SetActive(expansionCost<=gold);
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 1000, layerMask))
        {
            if(hit.transform.gameObject.layer == 10) 
            {
                if(hoveredDirtBlock != hit.transform.gameObject)
                {
                    if(hoveredDirtBlock != null)
                    {
                        hoveredDirtBlock.GetComponent<GroundSystem>().hoverOff();
                    }
                    hit.transform.gameObject.GetComponent<GroundSystem>().hoverOn();
                    hoveredDirtBlock = hit.transform.gameObject;
                }
            }
        }
        hoveredDirtBlock.GetComponentInChildren<Slider>().value=hoveredDirtBlock.GetComponent<GroundSystem>().waterLevel/hoveredDirtBlock.GetComponent<GroundSystem>().startingWaterLevel;

        if (Input.GetMouseButtonDown(0))
        {
            if (hoveredDirtBlock != null)
            {
                switch (currentTool)
                {
                    case 0:
                        if(gold>=seedPrices[plantType]){
                            print("Plant" + plantType +" on " + hoveredDirtBlock.name);
                            if(hoveredDirtBlock.GetComponent<GroundSystem>().sowPlant(seedID[plantType]))
                            { 
                                decGold(seedPrices[plantType]);
                            }
                            //Insert Planting Logic Here
                        } else {
                            Debug.Log("NO YOU ARE POOR");
                        }
                            break;
                    case 1:
                        print("Water " + hoveredDirtBlock.name + "'s Plant!");
                        hoveredDirtBlock.GetComponent<GroundSystem>().water();
                        //Insert Water Logic Here
                        break;
                    case 2:
                        print("Harvest Plants! from " + hoveredDirtBlock.name);
                        int harvestResult = hoveredDirtBlock.GetComponent<GroundSystem>().harvestPlant();
                        if(harvestResult>=0){
                            incGold(harvestResult);
                        }
                        
                        //Insert Harvesting Logic Here
                        break;
                    default:
                        print("Theres no tool tied to this value");
                        break;
                }
            }
            }
            
        }
    

    public void setTool(int i)
    {
        currentTool = i;
    }
    public int getTool()
    {
        return currentTool;
    }
    public void setPlantType(string s)
    {
        plantType = s;
    }
    public string getPlantType()
    {
        return plantType;
    }
    public void toggleSeedBar()
    {
        bool toggle=!SeedBar.activeSelf;
        SeedBar.SetActive(toggle);
    }
    public void setGold(int g)
    {
        gold=Mathf.Max(g,0);
        updateGoldText();
    }
    public void decGold(int g)
    {
        g=gold-g;
        gold=Mathf.Max(g,0);
        updateGoldText();
    }
    public void incGold(int g)
    {
        gold+=g;
        updateGoldText();
    }
    public int getGold(int g)
    {
        return gold;
    }
    public void updateGoldText()
    {  
        GoldText.text=gold+"g";
        foreach (KeyValuePair<string, GameObject> entry in seedButtons)
        {
            entry.Value.GetComponent<Button>().interactable = (seedPrices[entry.Key]<=gold);
        }
        expandPanel.SetActive(expansionCost<=gold);
    }
    
    public void buttonExpand()
    {
        if(expansionCost<=gold)
        {
        decGold(expansionCost);
        expansionCost+=50;
        expandPanel.GetComponentInChildren<Text>().text=expansionCost+"g";
        gridSystem.GetComponent<GridSystem>().Expand();
        }
    }

}
