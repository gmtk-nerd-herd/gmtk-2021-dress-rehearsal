using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSystem : MonoBehaviour
{
    public byte endGridSize = 32;
    public byte startGridSize = 2;

    public byte tileSize = 2;
    public float spaceBetweenTiles = 0;
    public GameObject tile;


    private GameObject[,] tiles;
    private byte stage;
    void Awake()
    {
        stage = startGridSize;
        tiles = new GameObject[endGridSize, endGridSize];
        for (int x = 0; x < endGridSize; x++)
        {
            for (int y = 0; y < endGridSize; y++)
            {
                tiles[x, y] = Instantiate(tile, new Vector3(x * (tileSize + spaceBetweenTiles), 0, y * (tileSize + spaceBetweenTiles)), Quaternion.identity, transform);
                if (x < startGridSize && y < startGridSize)
                {
                    tiles[x, y].GetComponent<GroundSystem>().activate();
                }
            }
        }
        Debug.Log("EXPANDED");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Expand() 
    {
        if(stage == 32) return;

        Camera.main.transform.gameObject.GetComponent<CameraPanOut>().updateCameraPosition();
        stage++;
        for (int x = 0; x < stage; x++)
        {
            for (int y = 0; y < stage; y++)
            {
                tiles[x, y].GetComponent<GroundSystem>().activate();
            }
        }
    }

}
