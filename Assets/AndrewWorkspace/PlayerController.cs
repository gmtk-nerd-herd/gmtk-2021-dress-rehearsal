using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform cam;
    CharacterController controller;
    Animator animator;
    public float speed = 6f;
    public float turnSmoothTime = 0.1f;
    public float speedSmoothTime = 0.1f;
    float turnSmoothVelocity;
    float speedSmoothVelocity;

    public float gravityValue = -10f;
    public float jumpHeight = 3f;
    Vector3 playerVelocity;
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        bool jump = Input.GetButtonDown("Jump");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        bool groundedPlayer = controller.isGrounded;
        float speedTrans;
        float blend = animator.GetFloat("Blend");

        if (groundedPlayer && playerVelocity.y < -1f)
        {
            playerVelocity.y = -1f;
        }

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x,direction.z)* Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y,targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            speedTrans = Mathf.SmoothDampAngle(blend,1,ref speedSmoothVelocity,speedSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            Vector3 moveDirection = Quaternion.Euler(0f,targetAngle,0f)*Vector3.forward;
            animator.SetFloat("Blend",speedTrans);
            controller.Move(moveDirection.normalized * speed * Time.deltaTime);   
        } else {
            speedTrans = Mathf.SmoothDampAngle(blend,0,ref speedSmoothVelocity,speedSmoothTime);
            animator.SetFloat("Blend",speedTrans);  
        }


        if (jump && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            Debug.Log("Jump!");
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }
}