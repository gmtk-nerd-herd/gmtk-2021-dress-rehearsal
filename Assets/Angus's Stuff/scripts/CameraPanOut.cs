using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanOut : MonoBehaviour
{
    // Start is called before the first frame update
    public Camera cam;
    private Vector3 cameraPosition;
    public int cameraMoveSpeed;
    void Start()
    {
        cam = GetComponent<Camera>();
        gameObject.transform.position = new Vector3(.8f, 5.8f, .64f);
        cameraPosition = cam.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(gameObject.transform.position, cameraPosition, cameraMoveSpeed * Time.deltaTime);
    }

    public void updateCameraPosition()
    {
        cameraPosition = new Vector3(cameraPosition.x + .85f, cameraPosition.y + 1.5f, cameraPosition.z + .625f);
    }
}
